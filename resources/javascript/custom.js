$(document).ready(function() {
  $('.ui.checkbox').checkbox();
  $('a.item').click(function(){
    $('.item').removeClass('active');
    $(this).addClass('active');
  });
  $('.ui.rating').rating();
  $('.rated.ui.rating').rating('disable');
  $('.ui.dropdown').dropdown();
  $('.tab-menu.menu .item').tab();
  $('.ui.sidebar').sidebar('attach events', '.launch.button');
  $('#triggerFile').on('click', function(e){
    e.preventDefault()
    $("#inputFile").trigger('click');
    $("#profilePictureInputFile").trigger('click');
  });
  $( "#offer-category" ).click(function() {
    $( ".offer-cat" ).slideToggle( "slow", function() {
      // Animation complete.
    });
  });
  $('.ui.modal').modal('attach events', '.rate-book', 'show');
  $('.ui.faq.accordion').accordion();

  var path = location.pathname.split("/")[1];
  if(path == "") path = 'home';
  $('#' + path).addClass('active');

  var active_category = location.pathname.split("/")[2];
  $('#' + active_category).addClass('active');

  $("#suggest_category_button").click(function(){
    category_suggest();
    return false;
  });
  
  $('.dropzone').filedrop({
    fallback_id: 'u_button',
    url: '/upload/book',
    paramname: 'book',
    withCredentials: true,
    data: {
      param1: 'value1',
    },
      headers: {          // Send additional request headers
        'header': 'value'
      },
      error: function(err, file) {
        switch(err) {
          case 'BrowserNotSupported':
          noty({text:"Brauzer HTML5 drag & drop dəstəkləmir", type:"error",  layout:"topCenter"})
          break;
          case 'TooManyFiles':
          // user uploaded more than 'maxfiles'
          noty({text:"Birdən artıq fayl yükləmək olmaz", type:"error",  layout:"topCenter"});

          break;
          case 'FileTooLarge':
          // program encountered a file whose size is greater than 'maxfilesize'
          // FileTooLarge also has access to the file which was too large
          // use file.name to reference the filename of the culprit file
          break;
          case 'FileTypeNotAllowed':
          // The file type is not in the specified list 'allowedfiletypes'
          break;
          case 'FileExtensionNotAllowed':
          // The file extension is not in the specified list 'allowedfileextensions'
          noty({text:"Bu fayl tipinə icazə yoxdur", type:"error",  layout:"topCenter"});
          break;
          default:
          break;
        }
      },
      allowedfiletypes: [],   // filetypes allowed by Content-Type.  Empty array means no restrictions
      allowedfileextensions: [".pdf"], // file extensions allowed. Empty array means no restrictions
      maxfiles: 1,
      maxfilesize: 20,    // max file size in MBs
      dragOver: function() {
        // user dragging files over #dropzone
        $(".dropzone").css("background", "#f2f2f2");
      },
      dragLeave: function() {
        // user dragging files out of #dropzone
        $(".dropzone").css("background", "white");
      },
      docOver: function() {
        // user dragging files anywhere inside the browser document window
      },
      docLeave: function() {
        // user dragging files out of the browser document window
      },
      drop: function() {
        // user drops file
        $(".dropzone").css("background", "white");
      },
      uploadStarted: function(i, file, len){
        // a file began uploading
        // i = index => 0, 1, 2, 3, 4 etc
        // file is the actual file of the index
        // len = total files user dropped
        // console.log(file.size / 1048576);
        // console.log(file.data)
        // $(".dropzone").append(file);
      },
      speedUpdated: function(i, file, speed) {
        // speed in kb/s
        $('#label_upload').html("Kitabınız yüklənir. Zəhmət olmasa gözləyin.");  
      },
      uploadFinished: function(i, file, response, time) {
        // $(".dropzone").append(file);
        // console.log(response);
        // response = $.parseJSON(response);
        if(response.status == "success") {
          $('#label_upload').html("Kitabınız yükləndi: " + file);  
          $('.dropzone').removeClass('dropzone');

          $("#book_file_id").attr('value', response.message);
        }
        else {
          $('#label_upload').html("Yüklənmə vaxtı xəta baş verdi. Yenidən yoxlayın.");  
        }
        // console.log(response.message);
      },
      globalProgressUpdated: function(progress) {
        $('#progress').width(progress+"%");
      },
      afterAll: function() {

      }
    });
});

$(document).ready(function(){
  if (window.location.hash == '#_=_') {
    history.replaceState ? history.replaceState(null, null, window.location.href.split('#')[0]) : window.location.hash = '';
  }
});

// category suggest!
function category_suggest() {
  var data = $("#suggest_category").serialize();
  if($("input[name='category']").val())
    $.ajax({
      type: "POST",
      url: "/suggest_category",
      data: data,
      cache:false,
      success: function(data){
        var res = $.parseJSON(data);
        if(res.status == "success") {
          $(".offer-cat").html("<center>Təşəkkür edirik! Sorğunuz nəzərə alınacaq.</center>");
        }
        else {
          noty({
            text:"Xəta baş verdi. Zəhmət olmasa yenidən yoxlayın.",  
            type:"warning", 
            layout:"topCenter"
          });
        }
      }
    });
  else {
    noty({
      text:"Kateqoriya xanası boş qala bilməz.",  
      type:"warning", 
      layout:"topCenter"
    });
  }
}

// ajax login
function login_ajax() {
  $("#login input").closest("div").removeClass("error");
  var username = $("#login-username").val();
  var password = $("#login-password").val();
  if(!username || username == "") $("#login-username").closest("div").addClass("error");
  else if(!password || password == "") $("#login-password").closest("div").addClass("error");
  else {
    $.ajax({
      url: "/user/login/check",
      type: "POST",
      data: {
        username: username, 
        password: password
      },
      cache: false,
      beforeSend: function() {
        $("#login").addClass("loading");
      },
      success: function(data){
        // console.log(data);
        var res = $.parseJSON(data);
        // console.log(res.status);
        if(res.status == "success") {
          // $(".login_success_message").show();
          location.href="/profile";
        }
        else {
          $("#login input").closest("div").addClass("error");
          if(res.message == 'not-confirmed') {
            noty({
              text:"Təsdiqlənmə poçtu göndərilmişdir. Zəhmət olmasa daxil olmadan qabaq e-mailnizi təsdiqləyin.",  
              type:"warning", 
              layout:"topCenter"
            });
          }
          else {
            noty({
              text:"İstifadəçi adı vəya şifrə yanlışdır.",  
              type:"warning", 
              layout:"topCenter"
            });
          }
        }
      } 
    });
  }
}

function IsEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}
function inputAlphabet(input) {
  var regex = /^[a-z0-9]+$/;
  return regex.test(input);
}

// registration
function register_ajax() {
  var name = $("#name").val();
  var surname = $("#surname").val();
  var username = $("#username").val();
  var email = $("#email").val();
  var email_again = $("#email-again").val();
  var password = $("#password").val();
  var terms_accepted = $("#conditions").is(':checked');

  if(!name || name == "") {
    $("#name").closest("div").addClass("error");
    noty({
      text:"Adınızı daxil edin.",  
      type:"warning", 
      layout:"topCenter"
    });
  }
  else if(!surname || surname == "") {
    $("#surname").closest("div").addClass("error");
    noty({
      text:"Soyadınızı daxil edin.",  
      type:"warning", 
      layout:"topCenter"
    });
  }
  else if(username.length < 6 || !inputAlphabet(username)) {
    $("#username").closest("div").addClass("error");
    noty({
      text:"İstifadəçi adı 6 hərfdən kiçik ola bilməz və ancaq hərfdən ibarət olmalıdır.",  
      type:"warning", 
      layout:"topCenter"
    });
  }
  else if(!email || email == "") {
    $("#email").closest("div").addClass("error");
    noty({
      text:"E-mail daxil edin.",  
      type:"warning", 
      layout:"topCenter"
    });
  }
  else if(!IsEmail(email)) {
    $("#email").closest("div").addClass("error");
    noty({
      text:"E-mail düzgün daxil edin.",  
      type:"warning", 
      layout:"topCenter"
    });
  }
  else if(email_again != email) {
    $("#email-again").closest("div").addClass("error");
    noty({
      text:"E-maillər uyğun gəlmir.",  
      type:"warning", 
      layout:"topCenter"
    });
  }
  else if(!password || password == "") {
    $("#password").closest("div").addClass("error");
    noty({
      text:"Şifrənizi daxil edin.",  
      type:"warning", 
      layout:"topCenter"
    });
  }
  else if(!terms_accepted || terms_accepted == "") {
    $("#conditions").closest("div").addClass("error");
    noty({
      text:"Zəhmət olmasa qaydalar və şərtlərlə tanış olun.",  
      type:"warning", 
      layout:"topCenter"
    });
  }
  else {
    var data = $("#registration").serialize();
    $.ajax({
      url: "/user/register",
      type: "POST",
      data: data,
      cache: false,
      success: function(data){
        var res = $.parseJSON(data);
        if(res.status == "success") {
          $(".success_message")
          .html("Congratulations! You have successfully registered! Check your e-mail to confirm your registration.");
          $(".success_message").show();
        }
        else {
          if(res.message == "username") {
            $("#username").closest("div").addClass("error");
            noty({
              text:"İstifadəçi adı artıq mövcuddur.",  
              type:"warning", 
              layout:"topCenter"
            });
          }
          else if(res.message == "email") {
            $("#email").closest("div").addClass("error");
            noty({
              text:"E-mail adı artıq mövcuddur.",  
              type:"warning", 
              layout:"topCenter"
            });
          }
          else
            noty({
              text:"Xəta baş verdi. Zəhmət olmasa yenidən yoxlayın.",  
              type:"warning", 
              layout:"topCenter"
            });
        }
      },
      error: function() {
        noty({
          text:"Xəta baş verdi. Zəhmət olmasa yenidən yoxlayın.",  
          type:"warning", 
          layout:"topCenter"
        });
      }
    });// you have missed this bracket
}
}

$(document).ready(function(){

  $("#register_button").click(function(e){
    e.preventDefault();
    register_ajax();
  });

  $(".success_message").hide();
  $(".login_success_message").hide();

  $("#login_button").click(function(e){
    e.preventDefault();
    login_ajax();
  });

  $("#review_submit").click(function(){
    var review = $("#review_text").val();
    var book = $("#review_book_id").val();
    var stars = 0;

    $(".rev_star").each(function(index){
      if($(this).hasClass('active'))
        stars++;
    });
  // console.log(review + " " + book + " " + stars);

  if(review && book && stars) {
    $.ajax({
      url: "/review/add",
      type: "POST",
      data: {
        review: review, 
        book_id: book,
        stars: stars
      },
      cache: false,
      success: function(data){
        // console.log(data);
        var res = $.parseJSON(data);
        if(res.status == "success") {
          $(".review.modal").modal('hide');
          $.ajax({
            type: 'POST',
            data: {
              review_id: res.message
            },
            url: '/review/get/added',
            success: function(res) {
              var comments = $(".b-comments").text();
              if(comments.indexOf('Rəy yoxdur!') >= 0)
                $('.b-comments').html(res);
              else
                $('.b-comments').prepend(res);
            }
          });
        }
        else {
         noty({
          text:"Xəta baş verdi, yenidən yoxlayın",  
          type:"warning", 
          layout:"topCenter"});
       }
     } 
   });
  }
  else {
    if(!review || review == "") {
      noty({
        text: 'Zəhmət olmasa şərh yazın', 
        type:'warning', 
        layout:'topCenter'
      });
    }
    else if(!stars || stars > 0) {
      noty({
        text:"Zəhmət olmasa bal verin",  
        type:"warning", 
        layout:"topCenter"});
    }
  }
});

$("#book_yukle").click(function(){
  var book_name = $("#book_name").val();
  var book_author = $("#book_author").val();
  var book_description = $("#book_description").val();
  var terms_accepted = $("#book_terms").is(':checked');

  var book_category  = [];
  $(".category_book").each(function(index){
    if($(this).is(':checked')) {
      // console.log($(this).val());
      book_category.push($(this).val());
    }
  });
    // console.log(book_category);
    // var book_category = [1, 4];
    var book_file_id = $("#book_file_id").val();
    var book_pic_id = $("#book_pic_id").val();
    


    if(book_name && book_author && book_pic_id && book_description && book_category.length != 0 && book_file_id && terms_accepted) {
      $.ajax({
        url: "/upload/insert",
        type: "POST",
        data: {
          book_name: book_name,
          book_author: book_author,
          book_description: book_description,
          book_category: book_category,
          book_file_id: book_file_id,
          book_pic_id: book_pic_id
        },
        cache: false,
        success: function(data){
          // console.log(data);
          var res = $.parseJSON(data);
          if(res.status == "success") {
            location.href="/book/" + res.message;
          }
          else {
           noty({text:"Xəta baş verdi, yenidən yoxlayın",  type:"warning", layout:"topCenter"});
         }
       } 

     });
    }
    else {
      if(!book_name || book_name == "") {
        noty({
          text: 'Zəhmət olmasa kitabın adını daxil edin', 
          type:'warning', 
          layout:'topCenter'
        });
      }
      else if(!book_description || book_description == "") {
        noty({
          text:"Zəhmət olmasa kitab haqqında bir şeylər yazın",  
          type:"warning", 
          layout:"topCenter"
        });
      }
      else if(!book_category || book_category == "") {
        noty({
          text:"Zəhmət olmasa kitabın kateqoriyasını seçin",  
          type:"warning", 
          layout:"topCenter"
        });
      }
      else if(!book_file_id || book_file_id == "") {
        noty({
          text:"Zəhmət olmasa kitabı yükləyin", 
          type:"warning",  
          layout:"topCenter"
        });
      }
      else if(!terms_accepted || terms_accepted == "") {
        noty({
          text:"Zəhmət olmasa qaydalar və şərtlərlə razı olun",  
          type:"warning", 
          layout:"topCenter"
        });
      }
    }
  });

});

$(document).ready(function(){
  $("#public_private").click(function(){
    $(this).children("span").toggle();
  });
});

// kateqoriya filr search results sehifesinde.
$(document).ready(function(){
  $("#category-order a").click(function(e){
    var kateqoriya = $(this).text();
    if(kateqoriya != "Hamısı") {
      $(".result_container").each(function(i){
        $(this).show();
        var c = $(this).attr("data-category");
        if (c.search(kateqoriya) == -1) {
          $(this).hide();
        }
      });
    }
    else {
      $(".result_container").each(function(i){
        $(this).show();
      });
    }
  });
});

// kateqoriya filr search results sehifesinde.
$(document).ready(function(){
  $("#author-order a").click(function(e){
    var muellif = $(this).text();
    if(muellif != "Hamısı") {
      $(".result_container").each(function(i){
        $(this).show();
        var a = $(this).attr("data-author");
        if (a.search(muellif) == -1) {
          $(this).hide();
          // console.log(a);
        }
      });
    }
    else {
      $(".result_container").each(function(i){
        $(this).show();
      });
    }
  });



  //book_image_upload
  function readURL(input, profilePicture) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function(e) {
        var form = document.getElementById('book-image');

        var formData = new FormData(form);
        $.ajax({
          url : "/upload/image",
          type: "POST",
          data: formData,
          success: function(response){
            var res = $.parseJSON(response);
            if(res.status == "success") {
              $("#book_pic_id").attr('value', res.message);
              if(profilePicture == 0)
                $(".addbook-l-img > img").attr("src", e.target.result);
              else {
                alert("uploaded");
                $(".user-l-img > img").attr("src", e.target.result);
              }
            }
            else
              noty({
                text:"Şəkil formatı düzgün deyil.", 
                type:"warning", 
                layout:"topCenter"
              });
            // console.log(res.status);
          },
          processData: false,
          contentType: false
        });
      };
      reader.readAsDataURL(input.files[0]);
    }
  }
  $("#inputFile").change(function() {
    readURL(this, 0);
  });
  $("#profilePictureInputFile").change(function() {
    readURL(this, 1);
  });

  $("#profileEditFormSubmit").click(function(){
    fullname = $("#fullname").val();
    email = $("#email").val();
    picture = $("#book_pic_id").val();
    password = $("#password").val();
    password2 = $("#password2").val();
    about = $("#about").val();
    if(password){
      if(password != password2){
        noty({
          text:"Parollar eyni deyil", 
          type:"warning", 
          layout:"topCenter"
        });
        return;
      }
    }
    if(!fullname || fullname==""){
      noty({
        text:"Zəhmət olmasa adınızı yazın", 
        type:"warning", 
        layout:"topCenter"
      });
      return;
    }
    else if(!email || email==""){
     noty({
      text:"Zəhmət olmasa emailinizi yazın", 
      type:"warning", layout:"topCenter"
    });
     return;
   } 
   else if(!picture || picture==""){
     noty({
      text:"Zəhmət olmasa şəklinizi seçin", 
      type:"warning", 
      layout:"topCenter"
    });
     return;
   }
   // console.log(fullname);
   // console.log(email);
   // console.log(picture);
   // console.log(about);
   $.ajax({
    url: "/profile/save",
    type: "POST",
    data: {
      fullname: fullname,
      email: email,
      picture: picture,
      about: about,
      password: password
    },
    success: function(data){
      // console.log(data);
      var res = $.parseJSON(data);
      if(res.status == "success") {
        location.href="/profile";
      }
      else {
        noty({
          text:"Xəta baş verdi, yenidən yoxlayın",  
          type:"warning", 
          layout:"topCenter"
        });
      }
    } 
  });
 });
});

// ----------------- ESAS DEYISHIKLIKLER --------------------

$(document).ready(function () {
  $('.category-list a').hide();
  var counter = 0;
  var limit = 15;
  $('.category-list a').each(function (i) {
    if(counter < limit) {
      $(this).show();
      counter++;
    }
    else
      $(this).hide();
  });

  $('.load-more-section').click(function(){
    counter = 0;
    $('.category-list a').hide();
    limit = limit + 15;
    $('.category-list a').each(function (i) {
      if(counter < limit) {
        $(this).fadeIn(1000);
        counter++;
      }
      else
        $(this).hide();
    });
    if(limit > $('.category-list a').length)
      $('.load-more-section').hide();
  });
});


$(document).ready(function(){
  // console.log(window.location.pathname);
  var pathname = window.location.pathname;
  var url = pathname.split('/');
  var category = url[url.length - 1];
  // console.log(category);
  var offset = 20;
  $("#load_more_category").click(function(e){
    e.preventDefault();
    $.ajax({
      type: "POST",
      url: "/category/load_more",
      data: {
        category: category,
        offset: offset
      },
      success: function(response){
        var res = $.parseJSON(response);
        // console.log(res.status);
        if(res.status == 'done') {
          $("#load_more_category").hide();
        }
        offset += 10;
        // console.log(res.message);
        $("#post_container").append(res.message);
      }, 
      error: function() {
        // console.log("error while loading.");
      }
    });
  });
});

$(document).ready(function(){
  var offset = 20;
  var keyword = $('.r_search input').val();
  $("#load_more_search").click(function(e){
    e.preventDefault();
    $.ajax({
      type: "POST",
      url: "/search/load_more",
      data: {
        keyword: keyword,
        offset: offset
      },
      success: function(response){
        var res = $.parseJSON(response);
        // console.log(res.status);
        if(res.status == 'done') {
          $("#load_more_search").hide();
        }    
        offset += 10;
        $("#post_container").append(res.message);        
      }, 
      error: function() {
        // console.log("error while loading.");
      }
    });
  });
});

$(document).ready(function(){
  // console.log(window.location.pathname);
  var pathname = window.location.pathname;
  var url = pathname.split('/');
  var author = url[url.length - 1];
  // console.log(author);
  var offset = 20;
  $("#load_more_author").click(function(e){
    e.preventDefault();
    $.ajax({
      type: "POST",
      url: "/author/load_more",
      data: {
        author: author,
        offset: offset
      },
      success: function(response){
        var res = $.parseJSON(response);
        // console.log(res.status);
        if(res.status == 'done') {
          $("#load_more_author").hide();
        }
        offset += 10;
        // console.log(res.message);
        $("#post_container").append(res.message);
      }, 
      error: function() {
        // console.log("error while loading.");
      }
    });
  });
});

$(document).ready(function(){
  // console.log(window.location.pathname);
  var pathname = window.location.pathname;
  var url = pathname.split('/');
  var username = url[url.length - 1];
  // console.log(username);
  var offset = 20;
  $("#load_more_username").click(function(e){
    e.preventDefault();
    $.ajax({
      type: "POST",
      url: "/userbooks/load_more",
      data: {
        username: username,
        offset: offset
      },
      success: function(response){
        var res = $.parseJSON(response);
        // console.log(res.status);
        if(res.status == 'done') {
          $("#load_more_username").hide();
        }
        offset += 10;
        // console.log(res.message);
        $("#post_container").append(res.message);
      }, 
      error: function() {
        // console.log("error while loading.");
      }
    });
  });
});

$(document).ready(function(){
  var url = window.location.pathname.split('/');
  var book_url = url[url.length - 1];
  var download_link = "http://e-book.az/download/" + book_url;
  $("#qr_download div").qrcode({
    "render": "div",
    "size": 150,
    "color": "#3a3",
    "text": download_link
  });
});

$(document).ready(function(){
  $('#copy_link').clipboard({
    path: '/resources/javascript/jquery.clipboard.swf',
    copy: function() {
      noty({
        text:"Kitabın yükləmə linki kopyalandı.", 
        type:"success", 
        layout:"topCenter"
      });
      return $('#copy_link_text').val();
    }
  });
  $("#copy_link_text").click(function(){
   $(this).select();
 });
});

$(document).on('submit', 'form#login', function(e){
  e.preventDefault();
  login_ajax();
});

$(document).on('submit', 'form#registration', function(e){
  e.preventDefault();
  register_ajax();
});

$(document).on('submit', 'form#suggest_category', function(e){
  e.preventDefault();
  category_suggest();
});

$(document).ready(function(){
  $(".add_my_library").click(function(e){
    e.preventDefault();

    var book_id = $("#review_book_id").val();
    if(book_id) {
      $.ajax({
        url: "/library/add",
        type: "POST",
        data: {
          book_id: book_id
        },
        success: function(response) {
          var res = $.parseJSON(response);
          if(res.status == "success") {
            noty({
              text:"Kitab My Library-a əlavə olundu.", 
              type:"success", 
              layout:"topCenter"
            });
            $(".add_my_library").addClass("remove_from_lib");
            $(".add_my_library").removeClass("add_my_library");
            $(".remove_from_lib").text("Remove from My Library");
          }
          else {
            noty({
              text:"Xəta baş verdi.", 
              type:"warning", 
              layout:"topCenter"
            });
          }
        },
        error: function() {
          noty({
            text:"Xəta baş verdi.", 
            type:"warning", 
            layout:"topCenter"
          });
        }
      });
    }
  });
});

$(document).ready(function(){
  $(".remove_from_lib").click(function(e){
    e.preventDefault();

    var book_id = $("#review_book_id").val();
    if(book_id) {
      $.ajax({
        url: "/library/remove",
        type: "POST",
        data: {
          book_id: book_id
        },
        success: function(response) {
          var res = $.parseJSON(response);
          if(res.status == "success") {
            noty({
              text:"Kitab My Library-dan silindi.", 
              type:"success", 
              layout:"topCenter"
            });
            $(".remove_from_lib").addClass("add_my_library");
            $(".remove_from_lib").removeClass("remove_from_lib");
            $(".add_my_library").text("Add to My Library");
          }
          else {
            noty({
              text:"Xəta baş verdi.", 
              type:"warning", 
              layout:"topCenter"
            });
          }
        },
        error: function() {
          noty({
            text:"Xəta baş verdi.", 
            type:"warning", 
            layout:"topCenter"
          });
        }
      });
    }
  });
});
$(document).ready(function(){
  $(".remove_from_library_prof").click(function(e){
    e.preventDefault();

    var row = $(this).closest("tr");
    var book_id = row.attr("book");
    // console.log(book_id);
    if(book_id) {
      $.ajax({
        url: "/library/remove",
        type: "POST",
        data: {
          book_id: book_id
        },
        success: function(response) {
          var res = $.parseJSON(response);
          if(res.status == "success") {
            noty({
              text:"Kitab My Library-dan silindi.", 
              type:"success", 
              layout:"topCenter"
            });
            row.html("");
            // console.log($("#my_library table tbody").text());
            if($("#my_library table tbody").text() == "") {
              $("#my_library table tbody").text("No books bro");
            }
          }
          else {
            noty({
              text:"Xəta baş verdi.", 
              type:"warning", 
              layout:"topCenter"
            });
          }
        },
        error: function() {
          noty({
            text:"Xəta baş verdi.", 
            type:"warning", 
            layout:"topCenter"
          });
        }
      });
    }
  });
});
$(document).ready(function(){
  $("#book_update").click(function(){
    var book_name = $("#book_name").val();
    var book_author = $("#book_author").val();
    var book_description = $("#book_description").val();
    var book_link = $("#book_link").val();
    
    var book_category  = [];
    $(".category_book").each(function(index){
      if($(this).is(':checked')) {
        // console.log($(this).val());
        book_category.push($(this).val());
      }
    });
    var book_pic_id = $("#book_pic_id").val();

    if(book_name && book_author && book_pic_id && book_description && book_category.length != 0) {
      $.ajax({
        url: "/book/update",
        type: "POST",
        data: {
          book_name: book_name,
          book_author: book_author,
          book_description: book_description,
          book_category: book_category,
          book_link: book_link,
          book_pic_id: book_pic_id
        },
        cache: false,
        success: function(data){
          // console.log(data);
          var res = $.parseJSON(data);
          if(res.status == "success") {
            location.href="/book/" + res.message;
          }
          else {
           noty({text:"Xəta baş verdi, yenidən yoxlayın",  type:"warning", layout:"topCenter"});
         }
       } 

     });
    }
    else {
      if(!book_name || book_name == "") {
        noty({
          text: 'Zəhmət olmasa kitabın adını daxil edin', 
          type:'warning', 
          layout:'topCenter'
        });
      }
      else if(!book_description || book_description == "") {
        noty({
          text:"Zəhmət olmasa kitab haqqında bir şeylər yazın",  
          type:"warning", 
          layout:"topCenter"
        });
      }
      else if(!book_author || book_author == "") {
        noty({
          text:"Zəhmət olmasa kitabın müəllifini qeyd edin.",  
          type:"warning", 
          layout:"topCenter"
        });
      }
      else if(!book_category || book_category == "") {
        noty({
          text:"Zəhmət olmasa kitabın kateqoriyasını seçin",  
          type:"warning", 
          layout:"topCenter"
        });
      }
    }
  });
});

$(document).ready(function(){
  $("#book_delete").click(function(){
    var book_link = $("#book_link").val();
    
    if(book_link) {
      $.ajax({
        url: "/book/delete",
        type: "POST",
        data: {
          book_link: book_link,
        },
        cache: false,
        success: function(data){
          // console.log(data);
          var res = $.parseJSON(data);
          if(res.status == "success") {
            location.href="/profile";
          }
          else {
           noty({text:"Xəta baş verdi, yenidən yoxlayın",  type:"warning", layout:"topCenter"});
         }
       } 

     });
    }
  });
});

$(document).ready(function(){
  $("#forgot_password").click(function(e){
    e.preventDefault();
    var email = $("#login-username").val();
    if(email) {
      $.ajax({
        url: "/forgot-password",
        type: "POST",
        data: {
          email: email
        },
        success: function(response) {
          var res = $.parseJSON(response);
          if(res.status == "success") 
            noty({
              text:"E-mail göndərildi.",  
              type:"success", 
              layout:"topCenter"
            });
          else {
            console.log(res.message);
            if(res.message == 'email-not-confirmed') {
              noty({
                text:"E-mail təsdiqlənməyib! Size mail gonderdik. Zehmet olmasa ilk once tesdiqleyin.",  
                type:"warning", 
                layout:"topCenter"
              });
            }
            else 
              noty({
                text:"E-mail mövcud deyil.",  
                type:"warning", 
                layout:"topCenter"
              });
          }
        },
        error: function() {
          noty({
            text:"Xəta baş verdi.",  
            type:"warning", 
            layout:"topCenter"
          });
        }
      });
    }
    else {
      $("#login-username").closest("div").addClass("error");
      noty({
        text:"Zəhmət olmasa giriş hissəsində e-mail vəya istifadəçi adınızı daxil edin.",  
        type:"warning", 
        layout:"topCenter"
      });
    }
  });
});
$(document).on('keyup', 'input', function() {
  $(this).closest("div").removeClass("error");
});