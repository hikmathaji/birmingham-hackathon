<?php

class Bloomberg {
  
  protected $url = "https://http-api.openbloomberg.com/request?ns=blp&service=refdata&type=ReferenceDataRequest";

  public function Bloomberg() {
    $this->do_curl();
  }

  private function do_curl() {

    $data = json_encode(array('securities' => 'PPL1954518 Index', 
                              'fields' => 'PERSON_BOARD_MEMBERSHIPS'));

    $ch = curl_init(); 
    curl_setopt($ch, CURLOPT_URL, $this->url); 
    curl_setopt($ch, CURLOPT_PORT , 443); 
    curl_setopt($ch, CURLOPT_VERBOSE, '1');
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
    curl_setopt($ch, CURLOPT_CAINFO,  getcwd().'/bloomberg.crt');
    curl_setopt($ch, CURLOPT_SSLCERT, getcwd().'/client.crt');
    curl_setopt($ch, CURLOPT_SSLKEY,  getcwd().'/client.key'); 
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data); 

    $ch_data = curl_exec($ch); 
    if(!curl_errno($ch)){ 
      $info = curl_getinfo($ch); 
      echo 'Took ' . $info['total_time'] . ' seconds to send a request to ' . $info['url']; 
    } else { 
      echo 'Curl error: ' . curl_error($ch); 
    } 

    curl_close($ch); 
    echo $ch_data;

  }

}

$b = new Bloomberg();


?>