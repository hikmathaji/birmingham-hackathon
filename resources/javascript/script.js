$(document).ready(function(){
  $('.spin').spin('hide');
  $("form").submit(function(){

    var token = $("#searchInput").val();
    console.log("Making the ajax call...");
    $.ajax({
      url:"doMagic.php",
      type:"POST",
      data:{
        "search":token
      },
      beforeSend: function() {
        $('.spin').spin('show');
        $("#loading").append("<div>Hold tight! It might take some time!</div>");
      },
      success: function(response) {
        console.log("Getting the result...");
        response = $.parseJSON(response);

        console.log(response.status);
        if(response.status == "error") {
          console.log(response.message);
          $("#loading").html("Oops! I'm broken. Message: " + response.message);
        }
        else {
          $("#map").html("");
          var map, csv;
          console.log("Creating the map...");
          require([
            "esri/tasks/query",
            "esri/tasks/QueryTask",
            "esri/map", 
            "esri/layers/FeatureLayer",
            "esri/Color",
            "esri/symbols/SimpleMarkerSymbol",
            "esri/renderers/SimpleRenderer",
            "esri/InfoTemplate",
            "esri/urlUtils",
            "dojo/domReady!"
          ], function(Query, QueryTask,
            Map, FeatureLayer, Color, SimpleMarkerSymbol, SimpleRenderer, InfoTemplate, urlUtils
          ) {

            map = new Map("map", {
              basemap: "gray",
              center: [ -60, -10 ],
              zoom: 4 
            });
            console.log("Adding the dots...")
            
            var orangeRed = new Color([238, 69, 0, 0.5]); 
            var marker = new SimpleMarkerSymbol("solid", 15, null, orangeRed);
            var renderer = new SimpleRenderer(marker);
            var template = new InfoTemplate("${type}", "${place}");
            var str = "1 = 2";
             $.each(response.message, function(i, code){
              str = str + " OR (COUNTRY = '"+ getCountryName(code.country)+"')";
             });
             console.log(str);
              featureLayer = new FeatureLayer("http://services.arcgis.com/oKgs2tbjK6zwTdvi/ArcGIS/rest/services/Major_World_Cities/FeatureServer/0");
              featureLayer.setDefinitionExpression(str);
              
              featureLayer.setRenderer(renderer);
         
              featureLayer.setInfoTemplate(template);
              map.addLayer(featureLayer);
            });
          }
        }
      });
    return false;
  });
});