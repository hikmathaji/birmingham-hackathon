<?php

class MotherHacker {
  private $key    = "0BB4E3ABAF4EBB3EAF7BE3AD323F3DDD";
  private $dbhost = 'venus.rjv.me';
  private $dbuser = 'remote';
  private $dbpass = 'heaven';
  private $max    = '100';

  private $conn   = null;
  private $list   = array();
  private $output = array();
  private $db_domains = array();
  private $db_countries = array();

  public function MotherHacker($search) {

    $this->errors_off();

    $this->db_connect();

    $url = "http://api.majestic.com/api/json?app_api_key=$this->key&cmd=SearchByKeyword&query=$search&scope=0&count=$this->max";
    $result = json_decode($this->do_curl_request($url));
    if(!($result->Code === "OK")) $this->kill("Can not connect to Majestic API!");
    $domains = $result->DataTables->Results->Data;  

    
    // get existing data from database.
    $sql_select = "SELECT domain, country FROM countries";
    $retval_select = mysql_query($sql_select, $this->conn) or $this->kill("SELECT SQL error: " .mysql_error());
    if(!$retval_select) $this->kill("SELECT ret SQL error: " .mysql_error());

    while($row = mysql_fetch_array($retval_select)) { 
      $this->db_domains[] = $row['domain']; 
      $this->db_countries[] = $row['country'];
    }

    $matches = $result->DataTables->Results->Headers->Matches;
    $limit = $matches < $this->max ? $matches : $this->max; 
    for ($i=0; $i < $limit; $i++) { 

      $domain  = $domains[$i]->Item;
      if(in_array($domain, $this->db_domains)) { 
        array_push($this->list, $this->db_countries[array_search($domain, $this->db_domains)]);
        continue;
      }

      $country = $this->get_country($domain);
      if($country) {
        array_push($this->list, $country);

        $sql_insert = "INSERT INTO countries (domain, country) VALUES ('$domain', '$country')";
        $retval_insert = mysql_query($sql_insert, $this->conn) or $this->kill("INSERT SQL error: " .mysql_error());
        if(!$retval_insert) $this->kill("INSERT ret SQL error: " .mysql_error());    
      }  

    }

    mysql_close($this->conn);
  }

  public function json() {
    foreach ($this->list as $key => $value) {
      $this->output[$value]++;
    }

    $well_this_is_the_last = array();
    foreach ($this->output as $key => $value) {
      $well_this_is_the_last[] = array('country' => $key, 'count' => $value);
    }

     

    return json_encode(array('status' => 'ok', 'message' => $well_this_is_the_last));
  }

  private function kill($message) {
    $response = array('status' => 'error', 'message' => $message);
    print json_encode($response);
    die();
  }

  private function db_connect() {
    // DB connection
    $this->conn = mysql_connect($this->dbhost, $this->dbuser, $this->dbpass);
    if(!$this->conn) {
      die('Could not connect: ' . mysql_error());
    }
    mysql_select_db('brumhack');
  }

  private function do_curl_request($url) {

    $curl = curl_init();
    // curl request to given url.
    curl_setopt_array($curl, array(
      CURLOPT_RETURNTRANSFER => 1, 
      CURLOPT_URL => $url,
      CURLOPT_USERAGENT => 'MotherHacker Bot',
      CURLOPT_TIMEOUT_MS => 50000
    ));

    $result = curl_exec($curl);
    curl_close($curl);
    
    if(!$result) $this->kill("Well, seems curl function didn't like the given url.");
    return $result;
  }

  private function get_country($domain) {
    $country_code = $this->get_country_code_by_name($this->alexa_country($domain));
    if($country_code) return trim($country_code);
    
    $country_code = trim($this->do_curl_request("http://ipinfo.io/" . gethostbyname($domain) . "/country"));
    if(strlen($country_code) === 2)
      return $country_code;
    else return null; 
  }

  private function get_country_code_by_name($country) {
    $sql_select = "SELECT code FROM country_code WHERE country = '$country'";
    $retval_select = mysql_query($sql_select, $this->conn) or $this->kill("country SELECT SQL error: " .mysql_error());
    if(!$retval_select) $this->kill("SELECT ret SQL error: " .mysql_error());
    $row = mysql_fetch_assoc($retval_select);
    if(mysql_num_rows($retval_select) > 0)
      return $row['code'];
    else
      return null;
  }

  private function alexa_country($url = '') {
    $alexa = 'http://data.alexa.com/data?cli=10&dat=snbamz&url=' .  $url;
    $xml = new SimpleXmlElement($alexa, LIBXML_COMPACT, true);
    $node = $xml->xpath('//COUNTRY');
    if(isset($node[0])) {
      $dom = dom_import_simplexml($node[0]);
      $country = $dom->getAttribute('NAME');
      return $country;
    }
    return null;
  }

  private function errors_off() {
    error_reporting(0);
    ini_set('display_errors', 0);
  }
}
?>